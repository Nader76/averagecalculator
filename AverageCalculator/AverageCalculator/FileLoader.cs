﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AverageCalculator
{
    class FileLoader
    {
        public FileLoader() { }

        public List<double> LoadFile(string _path) {
            StreamReader reader = new StreamReader(_path);

            List<double> numbers = new List<double>();

            while (!reader.EndOfStream)
            {
                double num;
                if (Double.TryParse(reader.ReadLine(), out num)) //catches non number characters
                    numbers.Add(num);   
            }

            return numbers;
        }

        public double CalculateAverage(List<double> _nums) { //Could just use List.Average(), but figured you'd want to see more work
            double average = 0;

            foreach (double n in _nums)
                average += n;

            average = average / _nums.Count();
            return average;
        }
    }
}
