﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AverageCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = "C:/Users/Michael Nader/OneDrive/Documents/Pre-Interview/C#/AverageCalculator/AverageCalculator/data.csv";
            FileLoader loader = new FileLoader();

            List<double> numbers = loader.LoadFile(path); //double so decimals can be read
            double average = loader.CalculateAverage(numbers);

            Console.WriteLine("Average: " + String.Format("{0:F1}", average)); //limit to one decimal place
        }
    }
}
